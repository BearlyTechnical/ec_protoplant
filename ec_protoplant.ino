/*
 * Protoplant 1.3
 * Primitive LUX meter for multiple sensors.
 */

#define NUM_SENSORS 16

#define SMOOTHING 0.25f //Smooth over ~4 samples

#define MPL_BIT0  8
#define MPL_BIT1  7
#define MPL_BIT2  6
#define MPL_BIT3  5
#define MPL_ENA   4

#define REF_RESISTOR 6800
#define LUX_SCALER 30413290
#define LUX_EXPONENT -1.2597

float smoothed_output[NUM_SENSORS];

//Naive calibration to account for absorption of the sensor casing and LDR-resistor manufacturing variances
float calib_multipliers[] = {
  14.0f,
  7.9f,
  14.7f,
  36.5f,
  12.7f,
  14.2f,
  20.2f,
  14.1f,
  16.3f,
  13.6f,
  20.0f,
  16.5f,
  17.8f,
  25.45f,
  26.5f,
  12.8f
};

void setup() {
  
  Serial.begin(115200);
  
  //Set pins to appropriate modex.
  pinMode(A0, INPUT);
  pinMode(MPL_BIT0, OUTPUT);
  pinMode(MPL_BIT1, OUTPUT);
  pinMode(MPL_BIT2, OUTPUT);
  pinMode(MPL_BIT3, OUTPUT);
  pinMode(MPL_ENA,  OUTPUT);

  digitalWrite(MPL_ENA, LOW);
  
}

float adcToLux(int adc_reading){
  float v_ldr = (float)adc_reading / 1023.0 * 5;
  float lux = LUX_SCALER * pow((5 - v_ldr) / v_ldr * REF_RESISTOR, LUX_EXPONENT);
  return lux;
}

int analogReadMultiplexer(byte address){
  digitalWrite(MPL_BIT0, (address & 0b00001000));
  digitalWrite(MPL_BIT1, (address & 0b00000100));
  digitalWrite(MPL_BIT2, (address & 0b00000010));
  digitalWrite(MPL_BIT3, (address & 0b00000001));
  delay(2);
  return analogRead(A0);
}

void loop() {
  
  //Read sensors to memory
  for(int i=0; i<NUM_SENSORS; i++) {
    smoothed_output[i] = smoothed_output[i]*(1-SMOOTHING) + calib_multipliers[i]*adcToLux(analogReadMultiplexer(i))*(SMOOTHING);
  }
  
  //Print out most recent values
  for(int i=0; i<NUM_SENSORS; i++) {
    if(i!=0) Serial.print(",");
    Serial.print(smoothed_output[i]);
  }
  Serial.println();
  
}
