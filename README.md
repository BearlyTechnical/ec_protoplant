# Protoplant

Light measuring device for greenhouse setups

## Running the visualizer

Download [Processing](https://processing.org/) and load protoplant.pde into it. After that, check that the Arduino is connected and run the sketch.