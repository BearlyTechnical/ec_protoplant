import processing.serial.*;

static int num_sensors = 16;
static int offset_x = 30, offset_y = 20;
static int sensor_tag_size = 64;
static int graph_offset_x = 30, graph_height = 200, graph_offset_y = (num_sensors/8)*(sensor_tag_size + offset_y) + offset_y;
static int window_width = 600, window_height = 480;

Serial ser;
String inString;
float[] values = new float[num_sensors];
int[] order = new int[num_sensors];
float l_avg = 0;
float avg = 0;
float avg_diff = 0;
float lum_tot = 0;
float fluct = 0;

File save_file = null;

void settings(){
  size(window_width, window_height);  
}

void setup() { 
  for(int i=0; i < num_sensors; i++){
    values[i] = 0;
    order[i] = i;
  }
  String[] serials = Serial.list();
  printArray(serials);
  ser = new Serial(this, serials[serials.length-1], 115200); 
  ser.bufferUntil(10); //buffer until a linefeed symbol has been found 
} 
 
void draw() {
  
  background(32);
  
  fill(16);
  stroke(20);
  rect(graph_offset_x, graph_offset_y, window_width-graph_offset_x*2, graph_height);
  
  fill(color(200,200,200));
  
  int ly_avg = graph_offset_y + (int)(graph_height - avg/values[order[0]] * graph_height);
  stroke(180,180,0);
  fill(255,255,0);
  line(graph_offset_x, ly_avg, window_width - graph_offset_x, ly_avg);
  text("average: " + avg, window_width - graph_offset_x - 120, ly_avg - 16);
  
  for(int i=0; i < num_sensors; i++){
    textSize(9);
    int rx = (i%8)*(sensor_tag_size+4);
    int ry = (offset_y + sensor_tag_size)*(i/8);
    float v = values[i];
    int c = (int)(v/values[order[0]] * 254);
    stroke(0,0,0);
    fill(color(c*0.3f,c,c*0.25f));
    rect(offset_x+rx, offset_y + ry, sensor_tag_size, sensor_tag_size, 5);
    
    boolean highlight = false;
    if(order[num_sensors-1] == i){
      stroke(color(254,0,0));
      fill(color(254,0,0));
      text("LOW", offset_x+sensor_tag_size*0.5f+rx, offset_y+ry+16);
      highlight = true;
    } else if(order[0] == i){
      stroke(color(0,0,254));
      fill(color(0,0,254));
      text("HIGH", offset_x+sensor_tag_size*0.5f+rx, offset_y+ry+16);
      highlight = true;
    }
    if(highlight){
      noFill();
      rect(offset_x+rx, offset_y + ry, sensor_tag_size, sensor_tag_size, 5);
      highlight = false;
    }
   
    if(c > 128){
      fill(0);
    } else {
      fill(254);
    }
    textSize(16);
    text(i+1, offset_x+4+rx, 18+offset_y+ry);
    textSize(10);
    text(v, offset_x+4+rx, 60+offset_y+ry);
  }
  
  fill(255,255,255);
  textSize(10);
  text("data fluctuation: " + fluct, graph_offset_x + 16, graph_offset_y + graph_height + 32);
  text("deviation from average: " + avg_diff, graph_offset_x + 16, graph_offset_y + graph_height + 48);
  text("total luminance: " + lum_tot, graph_offset_x + 16, graph_offset_y + graph_height + 64);
  //text("low_average: " + low_avg, offset_x + graph_offset_x + 250, offset_y + graph_offset_y + 82);
  
  stroke(color(255,128,0));
  
  for(int d = 0; d < num_sensors; d++){
    int x = graph_offset_x + d*((window_width-graph_offset_x*2)/num_sensors) + graph_offset_x;
    int y = graph_offset_y + (int)(graph_height - values[order[d]]/values[order[0]] * graph_height * 0.8f);

    if(d > 0){
      line(graph_offset_x + (d-1)*((window_width-graph_offset_x*2)/num_sensors) + graph_offset_x,
           graph_offset_y + (int)(graph_height - values[order[d-1]]/values[order[0]] * graph_height * 0.8f),
           x, 
           y);
    }
    ellipse(x,y,6,6);
    text(order[d]+1, x-4, y-5);
  }
  
  if(save_file != null){
    save(save_file.getAbsolutePath());
    save_file = null;
  }
  
}

void keyPressed() {
  if(key == 's'){
    selectOutput("Select a file to write to:", "fileSelected");
  }
}

void fileSelected(File selection) {
  if (selection != null) {
    save_file = selection;
  }
}

void serialEvent(Serial p) { 
  inString = p.readString();
  String[] data = splitTokens(inString, ",");
  int i=0;
  for(String d : data){
     if(values[i] == Float.NaN) values[i] = 0;
     try {
       float nf = Float.parseFloat(d);
       values[i] = nf*0.1f + values[i]*0.9f + (nf-values[i])*0.5f;
     } catch (Exception e){
     }
     i++;
     if(i >= num_sensors) break;
  }
  
  //Sort values for easier analysis
  for(int d = 0; d < num_sensors - 1; d++){
     for(int e = d+1; e < num_sensors; e++){
       if(values[order[e]] > values[order[d]]){
         int tmp = order[e];
         order[e] = order[d];
         order[d] = tmp;
       }
     }
  }
  
  //Averaging
  l_avg = avg;
  avg = 0;
  for(int d=0; d < num_sensors; d++){
    avg += values[d];
  }
  lum_tot = avg;
  avg /= (float)num_sensors;
  
  fluct = Math.abs(l_avg - avg);
  
  avg_diff = 0;
  for(int d=0; d < num_sensors; d++){
    avg_diff += Math.abs(avg - values[order[num_sensors-1-d]]);
  }
  
} 
